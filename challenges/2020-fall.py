import sys
import math


def debug(*args, **kwargs):
    print(*args, file=sys.stderr, flush=True, **kwargs)


def is_doable(inventory, recipe_delta):
    for inv, rec in zip(inventory, recipe_delta):
        if inv + rec < 0:
            return False
    return True


def main():
    # game loop
    while True:
        action_count = int(input())  # the number of spells and recipes in play
        recipes = []
        for i in range(action_count):
            # action_id: the unique ID of this spell or recipe
            # action_type: in the first league: BREW; later: CAST, OPPONENT_CAST, LEARN, BREW
            # delta_0: tier-0 ingredient change
            # delta_1: tier-1 ingredient change
            # delta_2: tier-2 ingredient change
            # delta_3: tier-3 ingredient change
            # price: the price in rupees if this is a potion
            # tome_index: in the first two leagues: always 0; later: the index in the tome if this is a tome spell, equal to the read-ahead tax
            # tax_count: in the first two leagues: always 0; later: the amount of taxed tier-0 ingredients you gain from learning this spell
            # castable: in the first league: always 0; later: 1 if this is a castable player spell
            # repeatable: for the first two leagues: always 0; later: 1 if this is a repeatable player spell
            action_id, action_type, delta_0, delta_1, delta_2, delta_3, price, tome_index, tax_count, castable, repeatable = input().split()
            action_id = int(action_id)
            delta = [int(delta_0), int(delta_1), int(delta_2), int(delta_3)]
            price = int(price)
            recipes.append({"price": price, "delta": delta, "action_id": action_id, "action_type": action_type})
            tome_index = int(tome_index)
            tax_count = int(tax_count)
            castable = castable != "0"
            repeatable = repeatable != "0"

        recipes.sort(reverse=True, key=lambda x: x["price"])

        inv_0, inv_1, inv_2, inv_3, score = [int(j) for j in input().split()]
        my_inventory = [inv_0, inv_1, inv_2, inv_3]
        my_score = score
        inv_0, inv_1, inv_2, inv_3, score = [int(j) for j in input().split()]
        enemy_inventory = [inv_0, inv_1, inv_2, inv_3]
        enemy_score = score

        debug(f"{my_inventory}, {recipes}")
        for recipe in recipes:
            if is_doable(my_inventory, recipe["delta"]):
                print(f"BREW {recipe['action_id']}")
                continue
        print("WAIT")


if __name__ == "__main__":
    main()

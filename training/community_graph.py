def main():
    points = {}
    min_abs, max_abs = 0, 0
    min_ord, max_ord = 0, 0

    n = int(input())
    for i in range(n):
        x, y = [int(j) for j in input().split()]
        points[str_point(x, y)] = True

        if x > max_abs:
            max_abs = x
        elif x < min_abs:
            min_abs = x

        if y > max_ord:
            max_ord = y
        elif y < min_ord:
            min_ord = y

    print_graph(points, min_abs, max_abs, min_ord, max_ord)


def print_graph(points, min_abs, max_abs, min_ord, max_ord):
    for row in range(max_ord + 1, min_ord - 2, -1):
        for col in range(min_abs - 1, max_abs + 2):
            if points.get(str_point(col, row)):
                print('*', end='')
            elif row == 0 and col == 0:
                print('+', end='')
            elif row == 0:
                print('-', end='')
            elif col == 0:
                print('|', end='')
            else:
                print('.', end='')
        print()


def str_point(x, y):
    return str(x) + ';' + str(y)


if __name__ == '__main__':
    main()

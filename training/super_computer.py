task_number = int(input())
calculus = []
for i in range(task_number):
    start, duration = [int(j) for j in input().split(' ')]

    calculus.append({
        'start': start,
        'end': start + duration - 1,
    })

calculus.sort(key=lambda x: x['end'])
selected = 1
last_selected = calculus[0]

for calc in calculus[1:]:
    if calc['start'] > last_selected['end']:
        selected += 1
        last_selected = calc

print(selected)

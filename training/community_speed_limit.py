import sys
from collections import OrderedDict


def main():
    speed_limit = int(input())
    n = int(input())
    vehicles = OrderedDict()
    for i in range(n):
        plaque, distance, timestamp = input().split()
        distance = int(distance)
        timestamp = int(timestamp)
        vehicles.setdefault(plaque, []).append((distance, timestamp))

    found_excess = False
    for k, v in vehicles.items():
        overspeeds = find_over_speed(v, speed_limit)
        for overspeed in overspeeds:
            found_excess = True
            print(k, overspeed[1])

    if not found_excess:
        print('OK')


def find_over_speed(vehicle, speed_limit):
    distance, timestamp = vehicle[0]
    overspeeds = []

    for data in vehicle[1:]:
        dist_delta = data[0] - distance
        time_delta = (data[1] - timestamp) / 3600
        speed = dist_delta / time_delta
        if speed > speed_limit:
            overspeeds.append((speed, data[0]))
        distance, timestamp = data

    return overspeeds


if __name__ == '__main__':
    main()

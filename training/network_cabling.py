import sys
import math
import statistics

n = int(input())
verticals = []
horizontals = []
for i in range(n):
    x, y = [int(j) for j in input().split()]
    horizontals.append(x)
    verticals.append(y)

median = round(statistics.median(verticals))

total = max(horizontals) - min(horizontals)
for y in verticals:
    total += abs(y - median)

print(total)

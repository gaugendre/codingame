package main

import (
	"fmt"
	"math"
)

func main() {
	var L, H int
	fmt.Scan(&L, &H)
	digitsToMaya := make(map[int]string)
	for i := 0; i < 20; i++ {
		digitsToMaya[i] = ""
	}

	for i := 0; i < H; i++ {
		var numeral string
		fmt.Scan(&numeral)
		for j := 0; j < 20; j++ {
			for k := 0; k < L; k++ {
				digitsToMaya[j] += string(numeral[j*L+k])
			}
			digitsToMaya[j] += "\n"
		}
	}

	mayaToDigits := make(map[string]int)
	for number, mayaDigit := range digitsToMaya {
		mayaToDigits[mayaDigit] = number
	}

	firstOperand := extractDigit(H, mayaToDigits)
	secondOperand := extractDigit(H, mayaToDigits)

	var operation string
	fmt.Scan(&operation)
	var result int
	if operation == "+" {
		result = firstOperand + secondOperand
	} else if operation == "-" {
		result = firstOperand - secondOperand
	} else if operation == "*" {
		result = firstOperand * secondOperand
	} else if operation == "/" {
		result = firstOperand / secondOperand
	}
	fmt.Println(numberToMaya(result, digitsToMaya))
}

func pow(base, exponent int) int {
	return int(math.Pow(float64(base), float64(exponent)))
}

func extractDigit(H int, mayaToDigits map[string]int) int {
	var S1 int
	fmt.Scan(&S1)
	s1Sum := 0

	for power := S1/H - 1; power >= 0; power-- {
		mayaDigit := ""
		for i := 0; i < H; i++ {
			var line string
			fmt.Scan(&line)
			mayaDigit += line + "\n"
		}
		digit, present := mayaToDigits[mayaDigit]
		if !present {
			panic("Maya digit not found!")
		}
		s1Sum += digit * pow(20, power)
	}
	return s1Sum
}

func numberToMaya(number int, digitsToMaya map[int]string) string {
	if number < 20 {
		return digitsToMaya[number]
	}
	var maya []string
	for number > 0 {
		mayaDigit := digitsToMaya[number%20]
		maya = append(maya, mayaDigit)
		number = number / 20
	}
	mayaResult := ""
	for i := len(maya) - 1; i >= 0; i-- {
		mayaResult += maya[i]
	}
	return mayaResult
}

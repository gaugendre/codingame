import sys
import re


class Element:
    pass


class Primitive(Element):
    def __init__(self, value):
        self.value = value


class Block(Element):
    def __init__(self, lst=None):
        if lst is None:
            lst = []
        self.content = lst


class KeyValue(Element):
    def __init__(self, key, value):
        self.key = key
        self.value = value


def parse(cgx_input):
    return cgx_input


def main():
    n = int(input())
    cgx_input = ''
    for i in range(n):
        cgx_input += input().strip()

    output = parse(cgx_input)

    print(output)

if __name__ == '__main__':
    main()

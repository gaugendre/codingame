def main():
    n = int(input())  # the number of relationships of influence
    influences = {}
    for i in range(n):
        # x: a relationship of influence between two people (x influences y)
        x, y = [int(j) for j in input().split()]
        influences.setdefault(x, []).append(y)

    all_lengths = [dfs(influences, x) for x in influences]
    print(max(all_lengths))


def dfs(graph, node, depth=1):
    for succ in graph[node]:
        if succ in graph:
            return dfs(graph, succ, depth + 1)
    return depth + 1


if __name__ == '__main__':
    main()

import sys
import math


def main():
    oods_number = int(input())
    gift_cost = int(input())
    budgets = []
    for i in range(oods_number):
        budgets.append(int(input()))

    budgets.sort()
    total = sum(budgets)

    if total < gift_cost:
        print("IMPOSSIBLE")
        return

    results = []
    for i, budget in enumerate(budgets):
        ideal = int((gift_cost - sum(results)) / (oods_number - i))
        if budget < ideal:
            results.append(budget)
        else:
            results.append(ideal)

    results.sort()

    for res in results:
        print(res)

if __name__ == '__main__':
    main()

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Record struct {
	floor, bars, beam string
}

func newRecord(row []string) *Record {
	barsFloat := row[1]
	beamFloat := row[2]
	floorFloat := row[3]
	return &Record{
		floorFloat,
		barsFloat,
		beamFloat,
	}
}

func (record *Record) update(row []string) {
	otherRecord := newRecord(row)
	if otherRecord.bars > record.bars {
		record.bars = otherRecord.bars
	}
	if otherRecord.floor > record.floor {
		record.floor = otherRecord.floor
	}
	if otherRecord.beam > record.beam {
		record.beam = otherRecord.beam
	}
}

func (record *Record) getCategory(category string) string {
	switch category {
	case "bars":
		return record.bars
	case "floor":
		return record.floor
	case "beam":
		return record.beam
	}
	return ""
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)

	records := make(map[string]*Record)

	scanner.Scan()
	gymnasts := strings.Split(scanner.Text(), ",")
	for _, gymnast := range gymnasts {
		records[gymnast] = &Record{}
	}

	scanner.Scan()
	categories := strings.Split(scanner.Text(), ",")
	var N int
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &N)

	for i := 0; i < N; i++ {
		scanner.Scan()
		row := strings.Split(scanner.Text(), ",")
		gymnastName := row[0]
		if record, ok := records[gymnastName]; ok {
			record.update(row)
		}
	}
	fmt.Fprintln(os.Stderr, gymnasts)
	fmt.Fprintln(os.Stderr, categories)
	for gymnast, record := range records {
		fmt.Fprintf(os.Stderr, "%s %#v", gymnast, record)
	}

	for _, gymnast := range gymnasts {
		var line []string
		for _, category := range categories {
			record := records[gymnast]
			line = append(line, record.getCategory(category))
		}
		fmt.Println(strings.Join(line, ","))
	}
}

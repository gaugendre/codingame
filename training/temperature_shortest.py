from math import fabs as f
N=int(input())
if N==0:
    print(0)
    exit(0)
T=map(int, input().split())
b=next(T)
for t in T:
    if f(t)<f(b):b=t
    if b<0 and f(b)==t:b=t
print(b)
import sys
import math


def main():
    n = int(input())

    people = {}
    for i in range(n):
        xi, yi = [int(j) for j in input().split()]
        pxi = people.setdefault(xi, set())
        pyi = people.setdefault(yi, set())
        pxi.add(yi)
        pyi.add(xi)

    # perr('people', people)

    min_length = math.inf
    # perr('people', people)
    for start in people:
        res = {}
        dfs3(people, start, res)
        # perr('res', res)
        inner_max_length = max(res.values())

        if inner_max_length < min_length:
            min_length = inner_max_length

    print(min_length)


def dfs3(graph: dict, start: str, res: dict, depth: int = 0, visited: set = None) -> None:
    if visited is None:
        visited = set()
    visited.add(start)

    depth += 1

    nxt = set(graph.get(start, [])) - visited
    for succ in nxt:
        distance = res.get(succ)
        if (distance and depth < distance) or not distance:
            res[succ] = depth
        dfs3(graph, succ, res, depth, visited)


def perr(*values):
    print(*values, file=sys.stderr)


if __name__ == '__main__':
    main()

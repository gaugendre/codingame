from collections import OrderedDict


def main():
    mapping = OrderedDict({
        3: 'Fizz',
        5: 'Buzz'
    })
    for i in range(1, 101):
        s = ''
        for k, v in mapping.items():
            if i % k == 0:
                s += v

        if s == '':
            s = i

        print(s)


if __name__ == '__main__':
    main()

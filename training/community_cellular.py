def to_bin(dot_at_str):
    return str(dot_at_str).replace('@', '1').replace('.', '0')


def to_dot_at(bin_str):
    return str(bin_str).replace('1', '@').replace('0', '.')


def evolve(pattern, neighborhood):
    evolution = ''
    for i in range(len(pattern)):
        key = pattern[i-1] + pattern[i] + pattern[(i+1) % len(pattern)]
        evolution += neighborhood[key]
    return evolution


def main():
    rule = format(int(input()), 'b').zfill(8)
    neighborhood = {}
    for i, value in enumerate(rule[::-1]):
        neighborhood[format(i, 'b').zfill(3)] = value
    n_lines = int(input())
    start_pattern = to_bin(input())

    for _ in range(n_lines):
        print(to_dot_at(start_pattern))
        start_pattern = evolve(start_pattern, neighborhood)


if __name__ == '__main__':
    main()

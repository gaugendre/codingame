import sys
import math

MAP_RADIUS = 6000


class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distance_to(self, other):
        if other is None:
            return math.inf
        return ((self.x - other.x)**2 + (self.y - other.y)**2) ** 0.5


class Entity(Position):
    def __init__(self, id, x, y, radius, mass):
        super().__init__(x, y)
        self.id = id
        self.radius = radius
        self.mass = mass


class Vehicle(Entity):
    def __init__(self, id, x, y, player, mass, radius, vx, vy):
        super().__init__(id, x, y, radius, mass)
        self.player = player
        self.mass = mass
        self.friction = 0.2
        self.vx = vx
        self.vy = vy


class Reaper(Vehicle):
    def play(self, target):
        print("{} {} 250".format(target.x, target.y))


class Destroyer(Vehicle):
    def play(self, target):
        print('{} {} 250'.format(target.x, target.y))


class Tanker(Vehicle):
    def __init__(self, id, x, y, player, mass, radius, vx, vy, water, max_water):
        super().__init__(id, x, y, player, mass, radius, vx, vy)
        self.water = water
        self.max_water = max_water


class Wreck(Tanker):
    pass


def main():
    # Auto-generated code below aims at helping you parse
    # the standard input according to the problem statement.

    # game loop
    while True:
        my_score = int(input())
        enemy_score_1 = int(input())
        enemy_score_2 = int(input())
        my_rage = int(input())
        enemy_rage_1 = int(input())
        enemy_rage_2 = int(input())
        unit_count = int(input())
        my_reaper = None
        enemy_reaper_1 = None
        enemy_reaper_2 = None
        my_destroyer = None
        enemy_destroyer_1 = None
        enemy_destroyer_2 = None
        wrecks = []
        nearest_wreck = None
        shortest_wreck_distance = math.inf
        tankers = []
        nearest_tanker = None
        shortest_tanker_distance = math.inf
        for i in range(unit_count):
            unit_id, unit_type, player, mass, radius, x, y, vx, vy, extra, extra_2 = input().split()
            unit_id = int(unit_id)
            unit_type = int(unit_type)
            player = int(player)
            mass = float(mass)
            radius = int(radius)
            x = int(x)
            y = int(y)
            vx = int(vx)
            vy = int(vy)
            extra = int(extra)
            extra_2 = int(extra_2)

            if unit_type == 0:
                reaper = Reaper(unit_id, x, y, player, mass, radius, vx, vy)
                if player == 0:
                    my_reaper = reaper
                elif player == 1:
                    enemy_reaper_1 = reaper
                elif player == 2:
                    enemy_reaper_2 = reaper

            elif unit_type == 1:
                destroyer = Destroyer(unit_id, x, y, player, mass, radius, vx, vy)
                if player == 0:
                    my_destroyer = destroyer
                elif player == 1:
                    enemy_destroyer_1 = destroyer
                elif player == 2:
                    enemy_destroyer_2 = destroyer

            elif unit_type == 3:
                tanker = Tanker(unit_id, x, y, player, mass, radius, vx, vy, extra, extra_2)
                tankers.append(tanker)
                if nearest_tanker is None and tanker.water > 0:
                    nearest_tanker = tanker
                elif tanker.water > 0:
                    distance = tanker.distance_to(nearest_tanker)
                    if distance < shortest_tanker_distance:
                        shortest_tanker_distance = distance
                        nearest_tanker = tanker

            elif unit_type == 4:
                wreck = Wreck(unit_id, x, y, player, mass, radius, vx, vy, extra, extra_2)
                wrecks.append(wreck)
                if nearest_wreck is None:
                    nearest_wreck = wreck
                else:
                    distance = wreck.distance_to(nearest_wreck)
                    if distance < shortest_wreck_distance:
                        shortest_wreck_distance = distance
                        nearest_wreck = wreck

        print(locals(), file=sys.stderr)
        reaper_target = nearest_wreck or nearest_tanker
        my_reaper.play(reaper_target)
        my_destroyer.play(nearest_tanker)
        print("WAIT")


if __name__ == '__main__':
    main()

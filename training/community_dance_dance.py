def main():
    l = int(input())
    n = int(input())
    frequencies = {}
    for i in range(n):
        pattern, tempo = input().split()
        tempo = int(tempo)
        frequencies[tempo] = pattern

    card = []
    for i in range(1, l + 1):
        line = '0000'
        for tempo, pattern in frequencies.items():
            if i % tempo == 0:
                line = add_lines(line, pattern)
        card.append(line)

    for line in card[::-1]:
        print(line)


def add_lines(current_line, new_line):
    line = ''
    for cc, cl in zip(current_line, new_line):
        if cc == 'X' or cl == 'X':
            line += 'X'
        else:
            line += '0'

    return line

if __name__ == '__main__':
    main()

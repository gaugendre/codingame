import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

# the number of points used to draw the surface of Mars.
surface_n = int(input())

prev_x, prev_y = (-1, -1)
start_x, start_y, end_x, end_y = (-1, -1, -1, -1)

for i in range(surface_n):
    # land_x: X coordinate of a surface point. (0 to 6999)
    # land_y: Y coordinate of a surface point.
    # By linking all the points together in a sequential fashion, you form the surface of Mars.

    land_x, land_y = [int(j) for j in input().split()]
    if land_y == prev_y:
        start_x, start_y = prev_x, prev_y

    if start_x != -1 and land_y != prev_y:
        end_x, end_y = prev_x, prev_y

    prev_x, prev_y = land_x, land_y


# game loop
while True:
    # h_speed: the horizontal speed (in m/s), can be negative.
    # v_speed: the vertical speed (in m/s), can be negative.
    # fuel: the quantity of remaining fuel in liters.
    # rotate: the rotation angle in degrees (-90 to 90).
    # power: the thrust power (0 to 4).
    x, y, h_speed, v_speed, fuel, rotate, power = [int(i) for i in input().split()]

    if x < start_x:
        s_angle = -30
        s_power = 4
    elif x > end_x:
        s_angle = 30
        s_power = 4
    else:
        if h_speed > 10:
            s_angle = 22
            s_power = 4
        elif h_speed < -10:
            s_angle = -22
            s_power = 4
        else:
            if y > end_y + 500:
                s_power = 3
            else:
                s_power = 4
            s_angle = 0


    # rotate power. rotate is the desired rotation angle. power is the desired thrust power.
    print("{} {}".format(s_angle, s_power))
